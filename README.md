# DueDateCalculator

Calculates the due date of tasks based on working days and hours.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)

## Installation

###Pre-Condition
Bitbucket SSH key must be set.

Add as a new repository to Your project's composer.json

```
  "repositories": [
    {
      "type": "vcs",
      "url": "git@bitbucket.org:ZackRave-N/duedatecalculator.git"
    }
  ],
```

Add as composer dependency
```
composer require zackraven/duedate-calculator
```

## Usage

```
$dueDateCalculator = new DueDateCalculator(
	new WorkSchedule(
		new DateTime('9:00'),
		new DateTime('17:00'),
		//Day indexes 1 - Monday, 7 - Sunday
		[1, 2, 3, 4, 5],
		8
	)
);

echo $dueDateCalculator->calculate(new DateTime('2019-08-23 10:12:38'), 5)->format(DATE_ATOM);

```