<?php

namespace ZackRaveN\DueDateCalculator;

use DateTime;
use DateTimeInterface;

class WorkSchedule implements WorkScheduleInterface
{
	/** @var DateTime */
	private $startTime;

	/** @var DateTime */
	private $endTime;

	/** @var int[] */
	private $workDayIndexes;

	public function __construct(DateTime $startTime, DateTime $endTime, array $workDayIndexes)
	{
		$this->startTime       = $startTime;
		$this->endTime         = $endTime;
		$this->workDayIndexes  = $workDayIndexes;
	}

	public function isTimeOutOfSchedule(DateTimeInterface $dateTime): bool
	{
		$timeFormat = 'H:i:s';

		$timeStamp = strtotime($dateTime->format($timeFormat));

		return $timeStamp > strtotime($this->endTime->format($timeFormat))
			|| $timeStamp < strtotime($this->startTime->format($timeFormat));
	}

	public function isWorkDay(DateTimeInterface $dateTime): bool
	{
		return in_array($dateTime->format('N'), $this->workDayIndexes);
	}

	public function getStartHour(): int
	{
		return $this->startTime->format('H');
	}

	public function getEndHour(): int
	{
		return $this->endTime->format('H');
	}

	public function getWorkingHoursPerDay(): int
	{
		return $this->endTime->diff($this->startTime)->h;
	}
}