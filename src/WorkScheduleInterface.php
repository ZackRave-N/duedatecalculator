<?php

namespace ZackRaveN\DueDateCalculator;

use DateTimeInterface;

interface WorkScheduleInterface
{
	public function isTimeOutOfSchedule(DateTimeInterface $dateTime): bool;

	public function isWorkDay(DateTimeInterface $dateTime): bool;

	public function getStartHour(): int;

	public function getEndHour(): int;

	public function getWorkingHoursPerDay(): int;
}