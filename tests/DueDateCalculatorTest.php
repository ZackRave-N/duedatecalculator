<?php

namespace ZackRaveN\Tests\DueDateCalculator;

use DateTime;
use PHPUnit\Framework\Exception as PhpUnitException;
use PHPUnit\Framework\TestCase;
use ZackRaveN\DueDateCalculator\DueDateCalculator;
use ZackRaveN\DueDateCalculator\InvalidSubmitDateException;
use ZackRaveN\DueDateCalculator\WorkSchedule;

class DueDateCalculatorTest extends TestCase
{
	/** @var DueDateCalculator */
	private $dueDateCalculator;

	public function setUp()
	{
		$this->dueDateCalculator = new DueDateCalculator(
			new WorkSchedule(
				new DateTime('9:00'),
				new DateTime('17:00'),
				[1, 2, 3, 4, 5]
			)
		);
	}

	public function calculateProvider(): array
	{
		return [
			[
				new DateTime('2019-07-24 14:12:38'),
				16,
				new DateTime('2019-07-26 14:12:38')
			],
			[
				new DateTime('2019-07-26 09:12:38'),
				24,
				new DateTime('2019-07-31 09:12:38')
			],
			[
				new DateTime('2019-07-24 14:33:12'),
				4,
				new DateTime('2019-07-25 10:33:12')
			],
			[
				new DateTime('2019-07-24 16:33:12'),
				8,
				new DateTime('2019-07-25 16:33:12')
			],
			[
				new DateTime('2019-08-23 16:59:59'),
				8,
				new DateTime('2019-08-26 16:59:59'),
			],
			[
				new DateTime('2019-08-23 12:00:01'),
				5,
				new DateTime('2019-08-26 09:00:01')
			],
			[
				new DateTime('2019-08-23 12:00:00'),
				5,
				new DateTime('2019-08-23 17:00:00')
			],
			[
				new DateTime('2019-08-23 12:00:01'),
				0,
				new DateTime('2019-08-23 12:00:01')
			],
		];
	}

	/**
	 * @dataProvider calculateProvider
	 *
	 * @param DateTime $submitDate
	 * @param int      $turnaroundTimeInHours
	 * @param DateTime $expectedResult
	 *
	 * @throws InvalidSubmitDateException
	 */
	public function testCalculate(
		DateTime $submitDate,
		int $turnaroundTimeInHours,
		DateTime $expectedResult
	) {
		$this->assertEquals(
			$expectedResult,
			$this->dueDateCalculator->calculate($submitDate, $turnaroundTimeInHours)
		);
	}

	public function invalidSubmitDateProvider(): array
	{
		return [
			[
				new DateTime('04:00'),
			],
			[
				new DateTime('18:11'),
			],
			[
				new DateTime('08:59:59'),
			],
			[
				new DateTime('17:00:01'),
			],
			[
				new DateTime('2019-08-24 16:50:12'),
			],
			[
				new DateTime('2018-02-10 12:00:00'),
			],
		];
	}

	/**
	 * @dataProvider invalidSubmitDateProvider
	 *
	 * @param DateTime $submitDate
	 *
	 * @throws InvalidSubmitDateException
	 * @throws PhpUnitException
	 */
	public function testInvalidSubmitDate(
		DateTime $submitDate
	) {
		$this->expectException(InvalidSubmitDateException::class);

		$dueDateCalculator = new DueDateCalculator(
			new WorkSchedule(new DateTime('09:00'), new DateTime('17:00'), [1, 2, 3, 4, 5])
		);

		$dueDateCalculator->calculate($submitDate, 0);
	}
}