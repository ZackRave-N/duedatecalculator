<?php

namespace ZackRaveN\DueDateCalculator;

use InvalidArgumentException;

class InvalidSubmitDateException extends InvalidArgumentException
{
}
