<?php

namespace ZackRaveN\DueDateCalculator;

use DateInterval;
use DatePeriod;
use DateTime;

class DueDateCalculator
{
	/** @var WorkScheduleInterface */
	private $workSchedule;

	public function __construct(WorkScheduleInterface $workSchedule)
	{
		$this->workSchedule = $workSchedule;
	}

	/**
	 * @param DateTime $submitDate
	 * @param int      $turnaroundTimeInHours
	 *
	 * @return DateTime
	 *
	 * @throws InvalidSubmitDateException
	 */
	public function calculate(
		DateTime $submitDate,
		int $turnaroundTimeInHours
	): DateTime {
		$this->assureSubmitDateWasInWorkingHours($submitDate);

		$dueDate = clone $submitDate;
		$dueDate->add($this->calculateTurnAroundTimeInterval($turnaroundTimeInHours));

		return $this->calculateDueDate($submitDate, $dueDate);
	}

	private function calculateDueDate(DateTime $submitDate, DateTime $dueDate): DateTime
	{
		return $this->increaseDueDateByNoneWorkingDays($submitDate, $this->modifyDueDateByWorkingHours($dueDate));
	}

	private function modifyDueDateByWorkingHours(DateTime $dueDate): DateTime
	{
		if(!$this->workSchedule->isTimeOutOfSchedule($dueDate))
		{
			return $dueDate;
		}

		$tempDueDate = clone $dueDate;
		$tempDueDate->setTime($this->workSchedule->getEndHour(), 0);

		$overTime = $tempDueDate->diff($dueDate);

		$this->incrementDateByOneDay($tempDueDate);

		$tempDueDate->setTime($this->workSchedule->getStartHour(), 0);
		$tempDueDate->add($overTime);

		return $tempDueDate;
	}

	private function increaseDueDateByNoneWorkingDays(DateTime $submitDate, DateTime $dueDate): DateTime
	{
		$period = new DatePeriod($submitDate, new DateInterval('P1D'), $dueDate);

		foreach($period as $inPeriodDay)
		{
			if(!$this->workSchedule->isWorkDay($inPeriodDay))
			{
				$this->incrementDateByOneDay($dueDate);
			}
		}

		while(!$this->workSchedule->isWorkDay($dueDate))
		{
			$this->incrementDateByOneDay($dueDate);
		}

		return $dueDate;
	}

	private function calculateTurnAroundTimeInterval(int $turnaroundTimeInHours): DateInterval
	{
		$days  = floor($turnaroundTimeInHours / $this->workSchedule->getWorkingHoursPerDay());
		$hours = $turnaroundTimeInHours % $this->workSchedule->getWorkingHoursPerDay();

		return new DateInterval(sprintf('P%dDT%dH', $days, $hours));
	}

	private function incrementDateByOneDay(DateTime $dateTime): DateTime
	{
		return $dateTime->add(new DateInterval('P1D'));
	}

	/**
	 * @param DateTime $submitDate
	 *
	 * @throws InvalidSubmitDateException
	 */
	private function assureSubmitDateWasInWorkingHours(DateTime $submitDate)
	{
		if(
			!$this->workSchedule->isWorkDay($submitDate)
			|| $this->workSchedule->isTimeOutOfSchedule($submitDate)
		)
		{
			throw new InvalidSubmitDateException(
				sprintf(
					'Submit date did not occur during working hours. (%s)',
					$submitDate->format(DATE_W3C)
				)
			);
		}
	}
}